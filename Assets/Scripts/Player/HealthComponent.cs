﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HealthComponent : MonoBehaviour
{
    private float health = 4;
    public Text livesCountText;

    public void Start()
    {
        livesCountText.text = "Lives: " + health.ToString();
    }

    public void Update()
    {
        if (health == 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
    public void DecrimentHealth()
    {
        health--;
        livesCountText.text = "Lives: " + health.ToString();
    }
}
