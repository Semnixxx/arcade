﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFlyCheck : MonoBehaviour
{
    private bool isFly = false;

    public void ChecngeIsFly()
    {
        if (isFly)
        {
            isFly = false;
        }
        else
        {
            isFly = true;
        }
    }

    public bool GetIsFly()
    {
        return isFly;
    }
}
