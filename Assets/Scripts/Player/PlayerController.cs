﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    public float force;
    public float jumpForce;

    private bool isFly;

    private bool isActive = true;

    private void Start()
    {
        rb = gameObject.AddComponent<Rigidbody>();
        rb.mass = 2;
    }
    void Update()
    {
        if (isActive == true)
        {
            if (Input.GetKey(KeyCode.W))
            {
                rb.AddForce(GetComponent<CameraCreator>().thisCameraTransform.forward * force * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.A))
            {
                rb.AddForce(GetComponent<CameraCreator>().thisCameraTransform.right * -force * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.D))
            {
                rb.AddForce(GetComponent<CameraCreator>().thisCameraTransform.right * force * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.S))
            {
                rb.AddForce(GetComponent<CameraCreator>().thisCameraTransform.forward * -force * Time.deltaTime);
            }
            if (Input.GetButtonDown("Jump"))
            {
                isFly = rb.gameObject.GetComponent<PlayerFlyCheck>().GetIsFly();
                if (isFly == false)
                {
                    rb.AddForce(Vector3.up * jumpForce);
                    rb.gameObject.GetComponent<PlayerFlyCheck>().ChecngeIsFly();
                }

            }
            if (Input.GetButtonDown("Shift"))
            {
                force *= 3;
            }
            if (Input.GetButtonUp("Shift"))
            {
                force /= 3;
            }
        }
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        isFly = rb.gameObject.GetComponent<PlayerFlyCheck>().GetIsFly();
        if (isFly == true)
        {
            rb.gameObject.GetComponent<PlayerFlyCheck>().ChecngeIsFly();
        }
    }

    public void ChangeActive()
    {
        if (isActive == true)
        {
            isActive = false;
        }
        else if (isActive == false)
        {
            isActive = true;
        }
    }
}
