﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathVolume : MonoBehaviour
{
    private GameObject checkPoint;
    private Vector3 position;
    private HealthComponent healthComponent;
    private Rigidbody playerRB;
    public CheckPointHolder checkPointHolder;
    public DeletedObjectsHolder deletedObjectsHolder;
    public DisappearingFloarHolder disappearingFloarHolder;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            checkPoint = checkPointHolder.GetCurrentCheckPoint();
            deletedObjectsHolder.EnableDisabledObjects();
            deletedObjectsHolder.ResetArray();
            if (disappearingFloarHolder != null)
            {
                disappearingFloarHolder.ResetPriority();
            }
            position = checkPoint.transform.position;
            position.y += 0.5f;
            collision.gameObject.transform.position = position;
            healthComponent = collision.gameObject.GetComponent<HealthComponent>();
            healthComponent.DecrimentHealth();
            playerRB = collision.gameObject.GetComponent<Rigidbody>();
        }
    }
}
