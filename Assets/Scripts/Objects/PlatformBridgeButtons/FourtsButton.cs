﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FourtsButton : MonoBehaviour
{
    public PlatformBridgeHolder bridgeHolder;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            bridgeHolder.ChangePlatformFourtsButton();
        }
    }
}
