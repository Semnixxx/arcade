﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstButton : MonoBehaviour
{
    public PlatformBridgeHolder bridgeHolder;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            bridgeHolder.ChangePlatformFirstButton();
        }
    }
}
