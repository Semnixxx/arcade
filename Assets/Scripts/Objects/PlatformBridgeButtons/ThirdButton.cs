﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdButton : MonoBehaviour
{
    public PlatformBridgeHolder bridgeHolder;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            bridgeHolder.ChangePlatformThirdButton();
        }
    }
}
