﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisappearingFloarPlatform : MonoBehaviour
{
    public DeletedObjectsHolder deletedObjectHolder;
    public DisappearingFloarHolder disappearingFloarHolder;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            if (gameObject == disappearingFloarHolder.GetNextPlatform())
            {
                disappearingFloarHolder.ChangeNextPlatform();
            }
            else
            {
                gameObject.SetActive(false);
                deletedObjectHolder.AddDisableObject(gameObject);
                if (collision.gameObject.GetComponent<PlayerFlyCheck>().GetIsFly() == false)
                {
                    collision.gameObject.GetComponent<PlayerFlyCheck>().ChecngeIsFly();
                }
            }
        }
    }

}
