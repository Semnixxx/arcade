﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisappearingFloarHolder : MonoBehaviour
{
    private byte counter = 0;
    private GameObject[] priority;
    private GameObject nextPlatform;
    public GameObject platform1;
    public GameObject platform2;
    public GameObject platform3;
    public GameObject platform4;
    public GameObject platform5;
    public GameObject platform6;
    public GameObject platform7;
    public GameObject platform8;
    public GameObject platform9;

    private void Start()
    {
        priority = new GameObject[9] {platform8, platform5, platform6, platform9, platform8, platform7, platform4, platform5, platform2 };
        nextPlatform = priority[counter];
    }

    public GameObject GetNextPlatform()
    {
        return nextPlatform;
    }

    public void ChangeNextPlatform()
    {
        counter++;
        if (counter < priority.Length)
        {
            nextPlatform = priority[counter];
        }
    }

    public void ResetPriority()
    {
        counter = 0;
        nextPlatform = priority[counter];
    }
}
