﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisabledPlatform : MonoBehaviour
{
    public DeletedObjectsHolder deletedObjectHolder;

    public ParticleSystem explodeSystem;

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            explodeSystem.Play();
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider>().enabled = false;
            if (deletedObjectHolder != null)
            {
                deletedObjectHolder.AddDisableObject(gameObject);
            }
        }
    }
}
