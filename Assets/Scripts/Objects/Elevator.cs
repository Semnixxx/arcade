﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    public float force = 30;
    private Rigidbody playerRigidbody;
    public void OnTriggerStay(Collider other)
    {
        playerRigidbody = other.gameObject.GetComponent<Rigidbody>();
        playerRigidbody.AddForce(Vector3.up * force);
    }
}
