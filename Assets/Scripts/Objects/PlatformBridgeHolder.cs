﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBridgeHolder : MonoBehaviour
{
    public GameObject[] platformsArray;
    
    void Start()
    {
        for (int i = 0; i < 4; i++)
        {
            platformsArray[i].gameObject.SetActive(false);
        }
    }
    
    public void ChangePlatformFirstButton()
    {
        ChangePlatformActiviti(platformsArray[0], platformsArray[2], platformsArray[3]);
    }
    
    public void ChangePlatformSecondButton()
    {
        ChangePlatformActiviti(platformsArray[1], platformsArray[2]);
    }

    public void ChangePlatformThirdButton()
    {
        ChangePlatformActiviti(platformsArray[2], platformsArray[3]);
    }

    public void ChangePlatformFourtsButton()
    {
        ChangePlatformActiviti(platformsArray[2]);
    }

    private void ChangePlatformActiviti(params GameObject[] platform)
    {
        for (int i = 0; i < platform.Length; i++)
        {
            if (platform[i].activeSelf == true)
            {
                platform[i].SetActive(false);
            }
            else if (platform[i].activeSelf == false)
            {
                platform[i].SetActive(true);
            }
        }
    }
}
