﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisabledPlatformWithTimer : MonoBehaviour
{
    private float time = 1.5f;
    public DeletedObjectsHolder deletedObjectHolder;

    public ParticleSystem explodeSystem;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            StartCoroutine(Timer());
        }
    }

    public IEnumerator Timer()
    {
        yield return new WaitForSeconds(time);
        explodeSystem.Play();
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<BoxCollider>().enabled = false;
        deletedObjectHolder.AddDisableObject(gameObject);
    }
}
