﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointUpdater : MonoBehaviour
{
    public CheckPointHolder checkPointHolder;
    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            checkPointHolder.SetCheckPoint(gameObject);
        }
    }
}
