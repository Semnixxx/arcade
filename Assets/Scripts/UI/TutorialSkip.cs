﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialSkip : MonoBehaviour
{    
    public GameObject[] images;
    private byte index = 0;

    public GameObject player;
    public Text livesCountText;
    private bool skip = true;

    private void Start()
    {
        livesCountText.gameObject.SetActive(false);
        player.GetComponent<CameraRotation>().ChangeActive();
        player.GetComponent<PlayerController>().ChangeActive();
        foreach (var e in images)
        {
            e.SetActive(false);
        }
        images[index].SetActive(true);

        UnityEngine.Cursor.visible = false;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (index < images.Length-1)
            {
                images[index].SetActive(false);
                index++;
                images[index].SetActive(true);
            }
            else
            {
                images[index].SetActive(false);
                player.GetComponent<CameraRotation>().ChangeActive();
                player.GetComponent<PlayerController>().ChangeActive();
                livesCountText.gameObject.SetActive(true);
                gameObject.SetActive(false);
            }
        }
    }
}
