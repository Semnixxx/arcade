﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCreator : MonoBehaviour
{
    private Transform thisTransform;
    private Camera thisCamera;
    public Transform thisCameraTransform { set; get; }
    void Start()
    {
        thisCameraTransform = new GameObject("Camera Container").transform;
        thisCamera = thisCameraTransform.gameObject.AddComponent<Camera>();
        thisCamera.tag = "MainCamera";

        thisTransform = transform;

        thisCamera.gameObject.AddComponent<AudioListener>();
    }

    public Transform GetTransform()
    {
        return thisTransform;
    }
}
