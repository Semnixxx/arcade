﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    private const float Y_ANGLE_MIN = 0.0f;
    private const float Y_ANGLE_MAX = 150.0f;

    private float distance = 10.0f;
    private float currentX = 0.0f;
    private float currentY = 0.0f;

    private bool isActive = true;

    private void Update()
    {
        if (isActive == true)
        {
            currentX += Input.GetAxis("Mouse X");
            currentY += Input.GetAxis("Mouse Y") * (-1);

            currentY = ClampAngle(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);
        }
    }

    private void LateUpdate()
    {
        if (isActive == true)
        {
            Vector3 dir = new Vector3(0, 0, -distance);
            Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
            GetComponent<CameraCreator>().thisCameraTransform.position = GetComponent<CameraCreator>().GetTransform().position + rotation * dir;
            GetComponent<CameraCreator>().thisCameraTransform.LookAt(GetComponent<CameraCreator>().GetTransform().position);
        }
    }

    private float ClampAngle(float angle, float min, float max)
    {
        do
        {
            if (angle < -360)
            {
                angle += 360;
            }
            if (angle > 360)
            {
                angle -= 360;
            }
        } while (angle < -360 || angle > 360);
        return Mathf.Clamp(angle, min, max);
    }

    public void ChangeActive()
    {
        if (isActive == true)
        {
            isActive = false;
        }
        else if (isActive == false)
        {
            isActive = true;
        }
    }
}
