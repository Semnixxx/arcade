﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPlayer : MonoBehaviour
{
    private AudioSource hitSound;

    public void Awake()
    {
        hitSound = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        hitSound.Play();
        StartCoroutine(WaitForNewSound());
    }

    private IEnumerator WaitForNewSound()
    {
        yield return new WaitForSeconds(0.2f);
    }
}
