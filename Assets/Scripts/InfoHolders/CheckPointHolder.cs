﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointHolder : MonoBehaviour
{
    private GameObject currentCheckPoint;
    public void SetCheckPoint(GameObject newCheckPoint)
    {
        currentCheckPoint = newCheckPoint;
    }

    public GameObject GetCurrentCheckPoint()
    {
        return currentCheckPoint;
    }
}
