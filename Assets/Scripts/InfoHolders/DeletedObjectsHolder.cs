﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeletedObjectsHolder : MonoBehaviour
{
    private int counter = 0;
    private GameObject[] oldDisabledObjects;
    private GameObject[] currentDisabledObjects;

    private void Start()
    {
        oldDisabledObjects = new GameObject[counter];
        currentDisabledObjects = new GameObject[counter];
    }
    public void EnableDisabledObjects()
    {
        foreach (var e in currentDisabledObjects)
        {
            e.SetActive(false);
            e.gameObject.GetComponent<MeshRenderer>().enabled = true;
            e.gameObject.GetComponent<BoxCollider>().enabled = true;
            e.SetActive(true);
        }
    }
    public void AddDisableObject(GameObject newGameObject)
    {
        counter++;
        oldDisabledObjects = new GameObject[counter];
        oldDisabledObjects = currentDisabledObjects;
        currentDisabledObjects = new GameObject[counter];
        for (int i = 0; i < oldDisabledObjects.Length; i++)
        {
            currentDisabledObjects[i] = oldDisabledObjects[i];
        }
        currentDisabledObjects[counter - 1] = newGameObject;
    }

    public void ResetArray()
    {
        counter = 0;
        oldDisabledObjects = new GameObject[counter];
        currentDisabledObjects = new GameObject[counter];
    }
}
